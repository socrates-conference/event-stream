let stream = require('./lib/EventStream');
const EventStream = stream.EventStream;
const ReplayingEventStream = stream.ReplayingEventStream;
const EventPublisher = require('./lib/EventPublisher');
const EventTracingStream = require('./lib/EventTracingStream');
const StringifyTransform = require('./lib/StringifyTransform');

module.exports = {
  EventStream: EventStream,
  ReplayingEventStream: ReplayingEventStream,
  EventPublisher: EventPublisher,
  EventTracingStream: EventTracingStream,
  StringifyTransform: StringifyTransform
};