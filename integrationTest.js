const EventPublisher = require('./lib/EventPublisher');
const EventStream = require('./lib/EventStream').EventStream;
const StringifyTransform = require('./lib/StringifyTransform');
const EventTracingStream = require('./lib/EventTracingStream');
const logger = require('bunyan').createLogger({
  name: 'test',
  level: 'trace'
});

let read = new EventStream([{topic: 'test'}], 'zookeeper', '2181', 'test-read', logger);
read.on('error', err => logger.error(err));
read.startConsuming();

read.pipe(new EventTracingStream(logger)).pipe(new StringifyTransform()).pipe(process.stdout);

let write = new EventPublisher('test', 'zookeeper', '2181', logger);
write.on('error', err => logger.error(err));
process.stdin.pipe(write);

console.error("Type into the console and press enter. " +
  "int: Valid JSON is transformed into objects, everything else ignored.");