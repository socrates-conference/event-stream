const Transform = require('stream').Transform;

class StringifyTransform extends Transform {
  constructor() {
    super();
    this._readableState.objectMode = true;
    this._writableState.objectMode = true;
  }

  //noinspection JSUnusedGlobalSymbols, JSUnusedLocalSymbols
  _transform(chunk, encoding, callback) {
    this.push(JSON.stringify(chunk));
    callback();
  }
}

module.exports = StringifyTransform;