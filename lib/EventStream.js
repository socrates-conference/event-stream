const Readable = require('stream').Readable;
const kafka = require('kafka-node');
const ConsumerGroup = kafka.ConsumerGroup;
const Client = kafka.Client;
const Producer = kafka.Producer;

class EventStream extends Readable {
  constructor(_topics, _host, _port, _groupId, _logger) {
    super({'objectMode': true});

    this.topics = _topics;
    this.host = _host ? _host : 'localhost';
    this.port = _port ? _port : '2181';
    this.groupId = _groupId ? _groupId : 'kafka-node-group';

    this.logger = _logger ? _logger : {
      info: (message, ...obj) => console.log(message, obj),
      error: (message, ...obj) => console.error(message, obj)
    };

    this.client = this.createKafkaClient(this.host, this.port);
    this.consumer = this.createKafkaConsumer(this.host, this.port);

    this.consumer.on('error', this.errorHandler.bind(this));

    this.consumer.on('message', this.messageHandler.bind(this));
  }

  messageHandler(message) {
    this.logger.trace("messageHandler");
    let json;
    try {
      if (typeof message === 'string') {
        message = JSON.parse(message);
      }
      json = JSON.parse(message.value);
    } catch (e) {
      this.logger.trace('Ignoring invalid JSON object:', message);
    }
    if (json && !this.push(json)) {
      this.logger.trace("push was false, pausing");
      this.consumer.pause();
    }
  }

  errorHandler(err, data) {
    if (err) {
      let errName = err.name || err.valueOf();
      if (Array.isArray(errName)) {
        errName = errName[0];
      }
      if (errName === 'TopicsNotExistError') {
        this.topicDoesNotExist(err);
      } else if (errName === 'BrokerNotAvailableError') {
        this.brokerNotAvailable();
      } else {
        this.unrecoverable(err, data);
      }
    }
  }

  brokerNotAvailable() {
    this.logger.info('Kafka broker was not available. ' +
      'Reconnecting after 1 second.');
    setTimeout(this.consumer.connect.bind(this.consumer), 1000);
  }

  unrecoverable(err, data) {
    let out = 'Unrecoverable Kafka Connection Error:  ' + err;
    if (data) {
      out += '(' + data + ')';
    }
    this.logger.error(out);
    this.emit('error', err);
  }

  topicDoesNotExist(err) {
    let missingTopics = err.topics;
    if (missingTopics) {
      this.logger.info('Topic(s) "' + missingTopics + '" weren\'t found.' +
        ' Creating new topic(s) "' + missingTopics + '".');
      this.producer = this.createKafkaProducer(this.client);
      const that = this;
      this.producer.createTopics(err.topics, true, (err, data) => {
        if (!err) {
          that.topicCreated(missingTopics);
        } else {
          that.errorHandler(err, data);
        }
      });
    } else {
      this.logger.info('There was an issue with missing topics.', err);
    }
  }

  //noinspection JSUnusedGlobalSymbols
  _read() {
    this.logger.trace("read called");
    this.consumer.resume();
  }

  topicCreated(topics) {
    this.logger.info('Topics "' + topics + '" created.');
  }

  createKafkaClient(host, port) {
    return new Client(host + ':' + port);
  }

  createKafkaProducer(client) {
    return new Producer(client);
  }

  createKafkaConsumer(host, port) {
    return new ConsumerGroup(
      {
        host: host + ':' + port,
        autoCommit: true,
        paused: true,
        groupId: this.groupId
      },
      this.topics.reduce((all, item) => {
        all.push(item.topic);
        return all;
      }, []));
  }

  startConsuming() {
    this.consumer.resume();
  }
}

class ReplayingEventStream extends EventStream {
  createKafkaConsumer(host, port) {
    return new ConsumerGroup(
      {
        host: host + ':' + port,
        autoCommit: false,
        fromOffset: 'earliest',
        paused: true,
        groupId: this.groupId
      },
      this.topics.reduce((all, item) => {
        all.push(item.topic);
        return all;
      }, []));
  }
}

module.exports = {
  EventStream: EventStream,
  ReplayingEventStream: ReplayingEventStream
};