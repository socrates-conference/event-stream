const Writable = require('stream').Writable;
const kafka = require('kafka-node');
const timeout = 3; // 3 second timeout for kafka rebalancing issues

class EventPublisher extends Writable {
  constructor(_topic, _host, _port, _logger) {
    super();
    this.topic = _topic;
    this.host = _host ? _host : 'localhost';
    this.port = _port ? _port : '2181';

    this.logger = _logger ? _logger : {
      info: (message, obj) => console.log(message, obj),
      error: (message, obj) => console.error(message, obj),
      trace: (message, obj) => console.trace(message, obj)
    };
    this.client = this.client();
    this.retries = 0;
    this.maxRetries = timeout * 10;
    this.kafkaProducer = this.producer(this.client);
    this.kafkaProducer.on('error', this.errorHandler.bind(this));
    this.kafkaProducer.on('ready', () => {
      this.connected = true;
      this.emit('ready');
    });
  }

  errorHandler(err, ...data) {
    if (err) {
      let errName = err.name || err.valueOf();
      if (Array.isArray(errName)) {
        errName = errName[0];
      }
      if (errName === 'TopicsNotExistError') {
        this.topicDoesNotExist(err);
      } else if (errName === 'BrokerNotAvailableError') {
        this.brokerNotAvailable(data);
      } else {
        this.unrecoverable(err, data);
        this.emit('error', err);
      }
    }
  }

  brokerNotAvailable(data) {
    if (!this.connected && data) {
      this.logger.info('Kafka broker was not available. ' +
        'Reconnecting after 1 second.');
      setTimeout(this.kafkaProducer.connect.bind(this.consumer), 1000);
    } else {
      this.retries++;
      if (this.retries < this.maxRetries) {
        this.logger.info('Kafka broker was not available. ' +
          'Trying again after 100 milliseconds.');
        setTimeout(this._write.bind(this, data[0], data[1], data[2]), 100);
      } else {
        this.emit('error', new Error(
          'Timed out after ' + timeout + 's trying to recover from a BrokerNotAvailableError '));
      }
    }
  }

  unrecoverable(err, data) {
    let out = 'Unrecoverable Kafka Connection Error:  ' + err;
    if (data) {
      out += '(' + data + ')';
    }
    this.logger.error(out);
  }

  topicDoesNotExist(err) {
    let missingTopics = err.topics;
    if (missingTopics) {
      this.logger.info('Topic "' + missingTopics + '" wasn\'t found.' +
        ' Creating new topic "' + missingTopics + '".');
      this.kafkaProducer.createTopics(err.topics, true, (err, data) => {
        if (!err) {
          this.topicCreated(missingTopics);
        } else {
          this.errorHandler(err, data);
        }
      });
    } else {
      this.logger.info('There was an issue with missing topics.', err);
    }
  }

  topicCreated(topics) {
    this.logger.info('Topics "' + topics + '" created.');
  }

  client() {
    return new kafka.Client(this.host +
      ':' + this.port);
  }

  //noinspection JSMethodCanBeStatic
  producer(client) {
    return new kafka.HighLevelProducer(client);
  }

  //noinspection JSUnusedGlobalSymbols
  _write(chunk, encoding, cb) {
    if (typeof encoding === 'function') {
      cb = encoding;
    }
    const payload = [{
      topic: this.topic,
      messages: chunk.toString()
    }];

    this.kafkaProducer.send(payload, err => {
      if (err) {
        this.errorHandler(err, chunk, encoding, cb);
      } else {
        this.retries = 0;
        this.logger.trace('Sent event to Kafka:', payload);
        return cb();
      }
    });
  }
}

module.exports = EventPublisher;
