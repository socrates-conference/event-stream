const assert = require('assert');
const sinon = require('sinon');
const EventPublisher = require('../lib/EventPublisher');
const EventEmitter = require('events').EventEmitter;

describe('EventPublisher:', () => {

  let producerMock;
  beforeEach(() => {
    EventPublisher.prototype.client = () => {
    };
    producerMock = new EventEmitter();
    producerMock.send = sinon.stub().callsArgWith(1, null, 'success');
    EventPublisher.prototype.producer = () => producerMock;
  });

  it('should send a ready event when kafka is initialized',
    done => {
      const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
        error: sinon.spy(),
        trace: sinon.spy()
      });
      eventProducer.on('ready', () => done());
      producerMock.emit('ready');
    });

  it('should send an error event if kafka can not be initialized',
    done => {
      const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
        error: sinon.spy(),
        trace: sinon.spy()
      });
      eventProducer.on('error', () => done());
      producerMock.emit('error', new Error());
    });

  it('should log info message and try to reconnect when Kafka is not available',
    done => {
      const loggerMock = {info: sinon.spy(), error: sinon.spy()};
      const eventProducer = new EventPublisher('test', null, null, loggerMock);
      eventProducer.on('error', () => {});
      producerMock.connect = function() {
        done();
      };
      producerMock.emit('error', {name: 'BrokerNotAvailableError'});
      assert(loggerMock.info.called, 'should log an info message');
    });

  it('should log info messages, create topics and ' +
    'try to reconnect when topic does not exist',
    done => {
      producerMock.createTopics = (topic, async, cb) => {
        producerMock.connect();
        return cb();
      };
      producerMock.connect = () => done();

      const loggerMock = {info: sinon.spy(), error: sinon.spy()};

      const eventProducer = new EventPublisher(
        'test', null, null, loggerMock);
      eventProducer.on('error', () => {});

      producerMock.emit('error', {name: 'TopicsNotExistError', topics: [{topic: 'test'}]});
      assert(loggerMock.info.called, 'should log an info message');
    });

  it('should send a message to kafka', done => {
    const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
      error: sinon.spy(),
      trace: sinon.spy()
    });
    eventProducer.write('myTestMessage', err => {
      assert.ifError(err, 'event producer returned error in cb');
      assert.deepEqual(producerMock.send.args[0][0], [{
        topic: 'test',
        messages: 'myTestMessage'
      }], 'event producer was called, but format of kafka payload was wrong');
      done();
    });
  });

  it('should emit fatal errors when sending a message to kafka',
    done => {
      const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
        error: sinon.spy(),
        trace: sinon.spy()
      });
      eventProducer.connected = true;
      const expectedErr = new Error('error');
      producerMock.send = sinon.stub().callsArgWith(1, expectedErr);
      eventProducer.on('error',
        err => {
          assert.deepEqual(err, expectedErr, 'event producer did not emit expected error');
          done();
        });

      eventProducer.write('myTestMessage', () => {});
    });

  it('should retry sending a message to kafka when broker is not available',
    done => {
      const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
        info: sinon.spy(),
        error: sinon.spy(),
        trace: sinon.spy()
      });
      eventProducer.connected = true;
      const expectedErr = 'BrokerNotAvailableError';
      producerMock.send = sinon.stub();
      producerMock.send.onFirstCall().callsArgWith(1, expectedErr);
      producerMock.send.onSecondCall().callsArgWith(1, null);
      eventProducer.on('error', err => {
        assert.ifError(err);
      });

      eventProducer.write('myTestMessage', () => {
        assert.equal(eventProducer.retries, 0, 'should reset retries after successfully sending event');
        done();
      });
    });

  it('should retry sending a message to kafka until 3s timeout when broker is not available',
    done => {
      const eventProducer = new EventPublisher('test', 'eventbus', '2181', {
        info: sinon.spy(),
        error: sinon.spy(),
        trace: sinon.spy()
      });
      eventProducer.connected = true;
      eventProducer.maxRetries = 5;
      const expectedErr = 'BrokerNotAvailableError';
      producerMock.send = sinon.stub();
      producerMock.send.callsArgWith(1, expectedErr);
      eventProducer.on('error', () => {
        done();
      });

      eventProducer.write('myTestMessage', () => {
        assert.fail('should never successfully send');
      });
    });
})
;
