const assert = require('assert');
const EventStream = require('../lib/EventStream').EventStream;
const EventEmitter = require('events').EventEmitter;

const sinon = require('sinon');
let kafkaMock, producerMock, assignedHost, assignedPort,
  assignedLogger, consumer;

class TestableEventStream extends EventStream {
  createKafkaClient(host, port) {
    assignedHost = host;
    assignedPort = port;
    return kafkaMock;
  }

  createKafkaConsumer() {
    assignedLogger = this.logger;
    return kafkaMock;
  }

  createKafkaProducer() {
    return producerMock;
  }
}

class MockKafka extends EventEmitter {
  // noinspection JSUnusedGlobalSymbols
  pause() {}

  // noinspection JSUnusedGlobalSymbols
  resume() {}
}

describe('EventStream:', () => {
  beforeEach(() => {
    kafkaMock = new MockKafka();

  });

  it('should log an error when Kafka returns an unrecoverable error',
    done => {
      const loggerMock = {
        info: sinon.spy(), error: () => {
          done();
        }
      };
      consumer = new TestableEventStream(
        [{topic: 'test'}], null, null, null, loggerMock);
      kafkaMock.emit('error', {});
    });

  it('should log info message and try to reconnect when Kafka is not available',
    done => {
      const loggerMock = {info: sinon.spy(), error: sinon.spy()};
      consumer = new TestableEventStream(
        [{topic: 'test'}], null, null, null, loggerMock);
      kafkaMock.connect = function () {
        done();
      };
      kafkaMock.emit('error', {name: 'BrokerNotAvailableError'});
      assert(loggerMock.info.called, 'should log an info message');
    });

  it('should log info messages, create topics and ' +
    'try to reconnect when topic does not exist',
    done => {
      kafkaMock.connect = () => {
        done();
      };

      producerMock = {
        createTopics: (topics, async, cb) => {
          kafkaMock.connect();
          return cb();
        }
      };

      const loggerMock = {info: sinon.spy(), error: sinon.spy()};

      consumer = new TestableEventStream(
        [{topic: 'test'}], null, null, null, loggerMock);

      kafkaMock.emit('error', {name: 'TopicsNotExistError', topics: [{topic: 'test'}]});
      assert(loggerMock.info.called, 'should log an info message');
    });

  it('should log error message and when Kafka returns LeaderNotAvailable error',
    done => {
      producerMock = {
        createTopics: function () {
          kafkaMock.connect();
        }
      };

      const loggerMock = {info: () => done(new Error()), error: () => done()};

      consumer = new TestableEventStream(
        [{topic: 'test'}], null, null, null, loggerMock);

      kafkaMock.emit('error', {
        valueOf: function () {
          return 'LeaderNotAvailable';
        }
      });
    });

  it('should emit data when a message event is received',
    done => {
      consumer = new TestableEventStream([{topic: 'test'}], null, null, null, {
        trace: (msg, ...args) => console.info(msg, args)
      });
      consumer.on('data', data => {
        assert.equal('event', data.event);
        assert.equal('test', data.payload.test);
        done();
      });
      kafkaMock.emit('message',
        JSON.stringify({
          topic: 'test',
          value: '{"event": "event", "payload": {"test":"test"}}'
        }));
    });

  it('should ignore a message event with unreadable JSON',
    done => {
      consumer = new TestableEventStream([{topic: 'test'}], null, null, null, {
        trace: msg => {
          if (msg === 'Ignoring invalid JSON object:') {
            done();
          }
        }
      });
      consumer.on('data', () => {
        done(new Error());
      });

      kafkaMock.emit('message',
        JSON.stringify({
          value: {
            topic: 'ipt-events',
            value: 'unreadable json'
          }
        }));
    });

  it('should subscribe to localhost:2181 if no other address is specified',
    () => {
      consumer = new TestableEventStream([{topic: 'test'}]);
      assert.equal('localhost', assignedHost);
      assert.equal('2181', assignedPort);
    });

  it('should contain console logger if no other logger is specified',
    () => {
      consumer = new TestableEventStream([{topic: 'test'}]);
      assert(assignedLogger);
      assert(assignedLogger.hasOwnProperty('info') &&
        assignedLogger.info instanceof Function);
      assert(assignedLogger.hasOwnProperty('error') &&
        assignedLogger.info instanceof Function);
    });
});
